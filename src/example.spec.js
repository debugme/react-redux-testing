import React from 'react'
import { createStore } from 'redux'
import { createRenderer } from 'react-test-renderer/shallow'
import expect from 'expect'
import expectJSX from 'expect-jsx'

expect.extend(expectJSX)

const Greeting = ({ message }) => {
  return (<div className={message === 'hello' ? 'stuff' : 'chick'}><h1>{message}</h1></div>)
}

const reducer = (state = 0, action) => {
  if (action.type === 'increment')
    return state + 1
  if (action.type === 'decrement')
    return state - 1
  return state
}

const incrementAction = { type: 'increment' }
const decrementAction = { type: 'decrement' }

describe('Greeting component', () => {

  it('should render the correct html', () => {
    const renderer = createRenderer()
    const actualValue = renderer.render(<Greeting message='hello' />)
    const expectValue = <div className="stuff"><h1>hello</h1></div>
    expect(actualValue).toEqualJSX(expectValue)
  })

  it('should render the correct type', () => {
    const renderer = createRenderer()
    const actualValue = renderer.render(<Greeting message='hello' />)
    const expectValue = 'div'
    expect(actualValue.type).toEqual('div')
  })

  it('should render the correct classname', () => {
    const renderer = createRenderer()
    const actualValue = renderer.render(<Greeting message='hello' />)
    const expectValue = 'stuff'
    expect(actualValue.props.className).toEqual('stuff')
  })

  it('should render the correct conditional classname', () => {
    const renderer = createRenderer()
    const actualValue = renderer.render(<Greeting message='byebye' />)
    const expectValue = 'chick'
    expect(actualValue.props.className).toEqual(expectValue)
  })

  it('should update store correctly', () => {
    const store = createStore(reducer)
    const initialState = store.getState()
    expect(initialState).toEqual(0)
    store.dispatch(incrementAction)
    const incrementedState = store.getState()
    expect(incrementedState).toEqual(1)
    store.dispatch(decrementAction)
    const decrementedState = store.getState()
    expect(decrementedState).toEqual(0)
  })


})



